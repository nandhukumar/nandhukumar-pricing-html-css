let toggleId = document.getElementById("toggleButton");
let basicPriceId = document.getElementById("basicPrice");
let professionalPriceId = document.getElementById("professionalPrice");
let masterPriceId = document.getElementById("masterPrice");

toggleId.addEventListener("click", () => {
  if (toggleId.checked) {
    basicPriceId.textContent = 19.99;
    professionalPriceId.textContent = 24.99;
    masterPriceId.textContent = 39.99;
  } else {
    basicPriceId.textContent = 199.99;
    professionalPriceId.textContent = 249.99;
    masterPriceId.textContent = 399.99;
  }
});
